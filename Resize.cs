using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulator
{    
    public class Resize
    {
        public Font resizeFont(int dl1)
        {
            Form1 f1 = new Form1();

            float font;
            font = f1.label1.Font.Size;
            
            switch (dl1)
            {
                case 10:                                      
                    {
                        font -= 3;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 11:
                    {
                        font -= 5;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 12:
                    {
                        font -= 7;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 13:
                    {
                        font -= 9;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 14:
                    {
                        font -= 11;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 15:
                    {
                        font -= 13;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 16:
                    {
                        font -= 13;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 17:
                    {
                        font -= 13;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 18:
                    {
                        font -= 13;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 19:
                    {
                        font -= 13;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 20:
                    {
                        font -= 13;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 21:
                    {
                        font -= 14;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 22:
                    {
                        font -= 14;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 23:
                    {
                        font -= 15;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 24:
                    {
                        font -= 15;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 25:
                    {
                        font -= 15;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 26:
                    {
                        font -= 15;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 27:
                    {
                        font -= 15;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                case 28:
                    {
                        font -= 15;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }
                default:
                    {
                        font = 30;
                        f1.label1.Font = new Font(f1.label1.Font.Name, font,
                        f1.label1.Font.Style, f1.label1.Font.Unit);
                        break;
                    }        

            }

            return f1.label1.Font;

        }     

    }
}