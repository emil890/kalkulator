using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulator
{
    public partial class Form1 : Form
    {
        double number1, score = 0, temporary = 1;
        int pressAction = 1, zeroOne = 1, point = 1, m = 1, firstTime = 1;
        string numberTxt, sign;
        float font;

        Calculations calculations = new Calculations();
        Backspace back = new Backspace();
        Resize resize = new Resize();       

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    {
                        break;
                    }
                case Keys.NumPad0:
                    {
                        zero_Click(sender, e);
                        break;
                    }
                case Keys.D0:
                    {
                        zero_Click(sender, e);
                        break;
                    }
                case Keys.NumPad1:
                    {
                        one_Click(sender, e);
                        break;
                    }
                case Keys.D1:
                    {
                        one_Click(sender, e);
                        break;
                    }
                case Keys.NumPad2:
                    {
                        two_Click(sender, e);
                        break;
                    }
                case Keys.D2:
                    {
                        two_Click(sender, e);
                        break;
                    }
                case Keys.NumPad3:
                    {
                        three_Click(sender, e);
                        break;
                    }
                case Keys.D3:
                    {
                        three_Click(sender, e);
                        break;
                    }
                case Keys.NumPad4:
                    {
                        four_Click(sender, e);
                        break;
                    }
                case Keys.D4:
                    {
                        four_Click(sender, e);
                        break;
                    }
                case Keys.NumPad5:
                    {
                        five_Click(sender, e);
                        break;
                    }
                case Keys.D5:
                    {
                        five_Click(sender, e);
                        break;
                    }
                case Keys.NumPad6:
                    {
                        six_Click(sender, e);
                        break;
                    }
                case Keys.D6:
                    {
                        six_Click(sender, e);
                        break;
                    }
                case Keys.NumPad7:
                    {
                        seven_Click(sender, e);
                        break;
                    }
                case Keys.D7:
                    {
                        seven_Click(sender, e);
                        break;
                    }
                case Keys.NumPad8:
                    {
                        eight_Click(sender, e);
                        break;
                    }
                case Keys.D8:
                    {
                        eight_Click(sender, e);
                        break;
                    }
                case Keys.NumPad9:
                    {
                        nine_Click(sender, e);
                        break;
                    }
                case Keys.D9:
                    {
                        nine_Click(sender, e);
                        break;
                    }
                case Keys.Back:
                    {
                        backspace_Click(sender, e);
                        break;
                    }
                case Keys.Oemcomma:
                    {
                        comma_Click(sender, e);
                        break;
                    }
                case Keys.Oemplus:
                    {
                        result_Click(sender, e);
                        break;
                    }
                case Keys.OemMinus:
                    {
                        minus_Click(sender, e);
                        break;
                    }
                case Keys.Add:
                    {
                        plus_Click(sender, e);
                        break;
                    }
                case Keys.Subtract:
                    {
                        minus_Click(sender, e);
                        break;
                    }
                case Keys.Divide:
                    {
                        divide_Click(sender, e);
                        break;
                    }
                case Keys.Multiply:
                    {
                        multiplication_Click(sender, e);
                        break;
                    }
                case Keys.Decimal:
                    {
                        comma_Click(sender, e);
                        break;
                    }

            }

        }

        private void warning(object sender, EventArgs e)
        {
            multiplication.Enabled = divide.Enabled = plus.Enabled = minus.Enabled =
            root.Enabled = factorial.Enabled = percent.Enabled = result.Enabled = logarithm.Enabled =
            CE.Enabled = pi.Enabled = comma.Enabled = one.Enabled = square.Enabled = backspace.Enabled =
            two.Enabled = three.Enabled = four.Enabled = five.Enabled =
            six.Enabled = seven.Enabled = eight.Enabled = nine.Enabled = zero.Enabled = false;
            clear.BackColor = Color.Red;
        }

        private void check(object sender, EventArgs e)
        {
            if (label1.Text == "∞")
            {
                textBox1.Text = "";
                warning(sender, e);
            }

        }

        private void plus_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (textBox1.Text != "" && pressAction == 0)
            {
                textBox1.Text += " + ";
            }
            else if (sign == "=")
            {
                textBox1.Text = label1.Text + " + ";
            }

            numberTxt = label1.Text;
            number1 = Convert.ToDouble(numberTxt);

            if (firstTime == 1)
            {
                score = number1;
                score = Math.Round(score, 6);
                firstTime--;
            }
            else if (sign != "√" && sign != "sqr" && sign != "!" && sign != "%" && sign != "log")
            {
                if (pressAction == 1 && sign == "-" || pressAction == 1 && sign == "*" || pressAction == 1 && sign == "÷")
                {
                    int w = dl2 - 2;

                    for (int i = dl2 - 1; dl2 > w; dl2--)
                    {
                        textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                    }

                    textBox1.Text += "+ ";
                }
                else if (pressAction == 0)
                score = calculations.calculating(sign, score, number1);
            }                  

            numberTxt = Convert.ToString(score);
            label1.Text = numberTxt;           

            sign = "+";

            int dl3 = label1.Text.Length;
            label1.Font = resize.resizeFont(dl3);

            check(sender, e);

            m = 1;
            pressAction = 1;
            zeroOne = 0;
            point = 1;
        }

        private void minus_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (textBox1.Text == "" && pressAction == 0)
            {
                textBox1.Text += "- ";
            }
            else if (pressAction == 0)
            {
                textBox1.Text += " - ";
            }

            if (sign == "=")
            {
                textBox1.Text = label1.Text + " - ";
            }

            numberTxt = label1.Text;
            number1 = Convert.ToDouble(numberTxt);

            if (firstTime == 1)
            {
                score = number1;
                firstTime--;
            }
            else if (sign != "√" && sign != "sqr" && sign != "!" && sign != "%" && sign != "log")
            {
                if (pressAction == 1 && sign == "+" || pressAction == 1 && sign == "*" || pressAction == 1 && sign == "÷")
                {
                    int w = dl2 - 2;

                    for (int i = dl2 - 1; dl2 > w; dl2--)
                    {
                        textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);

                    }

                    textBox1.Text += "- ";
                }
                else if (pressAction == 0)
                score = calculations.calculating(sign, score, number1);
            }         

            numberTxt = Convert.ToString(score);
            label1.Text = numberTxt;

            int dl3 = label1.Text.Length;
            label1.Font = resize.resizeFont(dl3);

            check(sender, e);

            sign = "-";
            m = 1;
            pressAction = 1;
            zeroOne = 0;
            point = 1;
        }

        private void multiplication_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (textBox1.Text != "" && pressAction == 0)
            {
                textBox1.Text += " x ";
            }
            else if (sign == "=")
            {
                textBox1.Text = label1.Text + " x ";
            }

            numberTxt = label1.Text;
            number1 = Convert.ToDouble(numberTxt);
             

            if (firstTime == 1)
            {
                score = number1;
                firstTime--;
            }
            else if (sign != "√" && sign != "sqr" && sign != "!" && sign != "%" && sign != "log")
            {
                if (pressAction == 1 && sign == "+" || pressAction == 1 && sign == "-" || pressAction == 1 && sign == "÷")
                {
                    int w = dl2 - 2;

                    for (int i = dl2 - 1; dl2 > w; dl2--)
                    {
                        textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                    }

                    textBox1.Text += "x ";
                }
                else if (pressAction == 0)
                {
                    score = calculations.calculating(sign, score, number1);
                }
                
            }

            numberTxt = Convert.ToString(score);
            label1.Text = numberTxt;

            int dl3 = label1.Text.Length;
            label1.Font = resize.resizeFont(dl3);

            check(sender, e);

            sign = "*";
            m = 1;
            pressAction = 1;
            zeroOne = 0;
            point = 1;
        }

        private void divide_Click(object sender, EventArgs e)
        {            
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;          

            if (textBox1.Text != "" && pressAction == 0)
            {
                textBox1.Text += " ÷ ";
            }
            else if (sign == "=")
            {
                textBox1.Text = label1.Text + " ÷ ";
            }

            numberTxt = label1.Text;
            number1 = Convert.ToDouble(numberTxt);           

            if (firstTime == 1)
            {
                score = number1;
                firstTime--;
            }
            else if (sign != "√" && sign != "sqr" && sign != "!" && sign != "%" && sign != "log")
            {
                if (pressAction == 1 && sign == "+" || pressAction == 1 && sign == "*" || pressAction == 1 && sign == "-")
                {
                    int w = dl2 - 2;

                    for (int i = dl2 - 1; dl2 > w; dl2--)
                    {
                        textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                    }

                    textBox1.Text += "÷ ";
                }
                else if (pressAction == 0)
                {
                    score = calculations.calculating(sign, score, number1);
                }
               
            }

            numberTxt = Convert.ToString(score);
            label1.Text = numberTxt;

            int dl3 = label1.Text.Length;
            label1.Font = resize.resizeFont(dl3);

            if (sign == "÷" && number1 == 0)
            {
                textBox1.Text = "";
                font = label1.Font.Size;
                font -= 18;
                label1.Font = new Font(label1.Font.Name, font,
                label1.Font.Style, label1.Font.Unit);
                label1.Text = "Nieprawidłowe dane wejścia";

                warning(sender, e);
            }

            sign = "÷";

            m = 1;
            pressAction = 1;
            zeroOne = 0;
            point = 1;
        }

        private void CE_Click(object sender, EventArgs e)

        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;                      
           
            if (dl1 != 0 && dl2 != 0 && pressAction == 0 && sign != "√" && sign != "log" && sign != "sqr" && sign != "%" && sign != "!")
            {
                for (int i = dl2 - 1; dl1 > 0; dl2--)
                {
                    textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                    dl1--;
                }

                label1.Text = "0";

                pressAction = 1;
                zeroOne = 0;
                point = 1;
            }                       
            
        }

        private void square_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;
                
            if (pressAction == 1 && textBox1.Text == "")
            {
                textBox1.Text = "sqr(0)";
            }
            else if (pressAction == 0 && sign != "sqr")
            {
                for (int i = dl2 - 1; dl1 > 0; dl2--)
                {
                    textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                    dl1--;
                }

                textBox1.Text += "sqr(" + label1.Text + ")";
            }          

            numberTxt = label1.Text;
            number1 = Convert.ToDouble(numberTxt);

            if (firstTime == 1)
            {
                sign = "sqr";
                score = number1;
                score = calculations.calculating(sign, score, number1);
                firstTime--;
                numberTxt = Convert.ToString(score);
                label1.Text = numberTxt;
            }
            else if (pressAction == 0 && sign != "sqr")
            {
                temporary = number1 * number1;
                numberTxt = Convert.ToString(temporary);
                label1.Text = numberTxt;
                score = calculations.calculating(sign, score, temporary);
            }
            else if (label1.Text != "" && sign != "sqr")
            {
                textBox1.Text += "sqr(" + label1.Text + ")";
                temporary = number1 * number1;
                numberTxt = Convert.ToString(temporary);
                label1.Text = numberTxt;
                score = calculations.calculating(sign, score, temporary);
            }

            int dl3 = label1.Text.Length;
            label1.Font = resize.resizeFont(dl3);

            sign = "sqr";

            check(sender, e);

            pressAction = 0;
            zeroOne = 0;
        }    

        private void factorial_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;                    

            if (pressAction == 1 && textBox1.Text == "")
            {
                textBox1.Text += "0!";
            }
            else if (pressAction == 0 && sign != "!")
            {
                textBox1.Text += "!";
            }

            numberTxt = label1.Text;
            number1 = Convert.ToDouble(numberTxt);

            
            if (pressAction == 0 && sign != "!")
            {
                for (int i = 1; i <= number1; i++)
                {
                    temporary *= i;
                }
                numberTxt = Convert.ToString(temporary);
                label1.Text = numberTxt;
                score = calculations.calculating(sign, score, temporary);
                temporary = 1;
            }
            else if (label1.Text != "" && sign != "!")
            {
                for (int i = 1; i <= number1; i++)
                {
                    temporary *= i;
                }
                textBox1.Text += label1.Text + "!";
                numberTxt = Convert.ToString(temporary);
                label1.Text = numberTxt;
                score = calculations.calculating(sign, score, temporary);
                temporary = 1;
            }
            else if (firstTime == 1)
            {
                sign = "!";
                score = number1;
                score = calculations.calculating(sign, score, number1);
                firstTime--;
                numberTxt = Convert.ToString(score);
                label1.Text = numberTxt;
            }

            int dl3 = label1.Text.Length;
            label1.Font = resize.resizeFont(dl3);

            sign = "!";

            check(sender, e);

            pressAction = 0;
            zeroOne = 0;
        }

        private void logarithm_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (pressAction == 1 && textBox1.Text == "")
            {
                textBox1.Text += "log(" + label1.Text + ")";
            }
            else if (pressAction == 0 && sign != "log")
            {
                for (int i = dl2 - 1; dl1 > 0; dl2--)
                {
                    textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                    dl1--;
                }

                if (point == 0)
                {
                    textBox1.Text += "log(" + label1.Text + "0)"; 
                }
                else
                textBox1.Text += "log(" + label1.Text + ")";
            }     

            numberTxt = label1.Text;
            number1 = Convert.ToDouble(numberTxt);

            if (firstTime == 1 && number1 > 0)
            {               
                sign = "log";
                score = number1;
                score = calculations.calculating(sign, score, number1);
                firstTime--;
                numberTxt = Convert.ToString(score);
                label1.Text = numberTxt;
            }           
            else if (pressAction == 0 && sign != "log" && number1 > 0)
            {
                temporary = Math.Log10(number1);
                numberTxt = Convert.ToString(temporary);
                label1.Text = numberTxt;
                score = calculations.calculating(sign, score, temporary);
            }
            else if (label1.Text != "" && sign != "log" && number1 > 0)
            {
                textBox1.Text += "log(" + label1.Text + ")";
                temporary = Math.Log10(number1);
                numberTxt = Convert.ToString(temporary);
                label1.Text = numberTxt;
                score = calculations.calculating(sign, score, temporary);
            }
            else if (score == 0 || number1 < 0)
            {
                textBox1.Text = "";
                font = label1.Font.Size;
                font -= 18;
                label1.Font = new Font(label1.Font.Name, font,
                label1.Font.Style, label1.Font.Unit);
                label1.Text = "Nieprawidłowe dane wejścia";

                warning(sender, e);
            }

            sign = "log";

            pressAction = 0;
            zeroOne = 0;
        }

        private void percent_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (pressAction == 1 && textBox1.Text == "")
            {
                textBox1.Text = "0%";
            }
            else if (pressAction == 0 && sign != "%")
            {
                for (int i = dl2 - 1; dl1 > 0; dl2--)
                {
                    textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                    dl1--;
                }

                textBox1.Text += label1.Text + "%";
            }

            numberTxt = label1.Text;
            number1 = Convert.ToDouble(numberTxt);

            if (firstTime == 1)
            {
                sign = "%";
                score = number1;
                score = calculations.calculating(sign, score, number1);
                firstTime--;
                numberTxt = Convert.ToString(score);
                label1.Text = numberTxt;
            }
            else if (pressAction == 0 && sign != "%")
            {                
                temporary = number1 / 100;               
                numberTxt = Convert.ToString(temporary);
                label1.Text = numberTxt;
                score = calculations.calculating(sign, score, temporary);
            }
            else if (label1.Text != "" && sign != "%")
            {
                textBox1.Text += label1.Text + "%";
                temporary = number1 / 100;
                numberTxt = Convert.ToString(temporary);
                label1.Text = numberTxt;
                score = calculations.calculating(sign, score, temporary);
            }

            sign = "%";

            pressAction = 0;
            zeroOne = 0;
        }

        private void pi_Click(object sender, EventArgs e)
        {                   
            if (pressAction > 0)
            {
                label1.Text = "3,141592";
                textBox1.Text += "3,141592";
                pressAction--;
            }

            sign = "pi";
            zeroOne = 0;
            point = 1;
        }

        private void root_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (pressAction == 1 && textBox1.Text == "")
            {
                textBox1.Text += "√(0)";
            }
            else if (pressAction == 0 && sign != "√")           
            {
                for (int i = dl2 - 1; dl1 > 0; dl2--)
                {
                    textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                    dl1--;
                }

                textBox1.Text += "√(" + label1.Text + ")";
            }

            numberTxt = label1.Text;
            number1 = Convert.ToDouble(numberTxt);

            if (firstTime == 1)
            {
                sign = "√";
                score = number1;
                score = calculations.calculating(sign, score, number1);
                firstTime--;
                numberTxt = Convert.ToString(score);
                label1.Text = numberTxt;
            }
            else if (pressAction == 0 && sign != "√")
            {
                temporary = Math.Sqrt(number1);
                numberTxt = Convert.ToString(temporary);
                label1.Text = numberTxt;
                score = calculations.calculating(sign, score, temporary);
            }
            else if (label1.Text != "" && sign != "√")
            {
                textBox1.Text += "√(" + label1.Text + ")";
                temporary = Math.Sqrt(number1);
                numberTxt = Convert.ToString(temporary);
                label1.Text = numberTxt;
                score = calculations.calculating(sign, score, temporary);
            }

            sign = "√";

            pressAction = 0;
            zeroOne = 0;
        }

        private void one_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (label1.Text == "0" && textBox1.Text != "")
            {
                textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                textBox1.Text += "1";
                label1.Text = "1";
            }
            else if (label1.Text == "0" && dl2 == 0 && textBox1.Text != ""|| label1.Text == "Nieprawidłowe dane wejścia" || textBox1.Text == "0")
            {
                label1.Text = "1";
                textBox1.Text = "1";
            }
            else if (dl1 < 27 && sign != "=")
            {
                label1.Text += "1";
            }

            if (label1.Text == "Nieprawidłowe dane wejścia" || sign == "=")
            {
                clear_Click(sender, e);
                label1.Text = "1";
            }

            if (point != 0 && label1.Text == "1" && m > 0 && dl2 != 0 && dl1 < 20)
            {
                textBox1.Text = back.remove(textBox1.Text);
                textBox1.Text += "1";
                m--;
            }
            else if (dl1 < 20 && sign != "=")
            {
                textBox1.Text += "1";
            }

            if (sign != "" && pressAction == 1 && point == 1 && dl1 < 20)
            {
                label1.Text = "1";
            }

            int dl3 = label1.Text.Length;

            pressAction = 0;

            label1.Font = resize.resizeFont(dl3);
        }

        private void two_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (label1.Text == "0" && textBox1.Text != "")
            {
                textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                textBox1.Text += "2";
                label1.Text = "2";
            }
            else if (label1.Text == "0" && dl2 == 0 && textBox1.Text != "" || label1.Text == "Nieprawidłowe dane wejścia" || textBox1.Text == "0")
            {
                label1.Text = "2";
                textBox1.Text = "2";
            }
            else if (dl1 < 27 && sign != "=")
            {
                label1.Text += "2";
            }

            if (label1.Text == "Nieprawidłowe dane wejścia" || sign == "=")
            {
                clear_Click(sender, e);
                label1.Text = "2";
            }

            if (point != 0 && label1.Text == "2" && m > 0 && dl2 != 0 && dl1 < 20)
            {
                textBox1.Text = back.remove(textBox1.Text);
                textBox1.Text += "2";
                m--;
            }
            else if (dl1 < 20 && sign != "=")
            {
                textBox1.Text += "2";
            }

            if (sign != "" && pressAction == 1 && point == 1 && dl1 < 20)
            {
                label1.Text = "2";
            }

            int dl3 = label1.Text.Length;

            pressAction = 0;

            label1.Font = resize.resizeFont(dl3);
        }

        private void three_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (label1.Text == "0" && textBox1.Text != "")
            {
                textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                textBox1.Text += "3";
                label1.Text = "3";
            }
            else if (label1.Text == "0" && dl2 == 0 && textBox1.Text != "" || label1.Text == "Nieprawidłowe dane wejścia" || textBox1.Text == "0")
            {
                label1.Text = "3";
                textBox1.Text = "3";
            }
            else if (dl1 < 27 && sign != "=")
            {
                label1.Text += "3";
            }

            if (label1.Text == "Nieprawidłowe dane wejścia" || sign == "=")
            {
                clear_Click(sender, e);
                label1.Text = "3";
            }

            if (point != 0 && label1.Text == "3" && m > 0 && dl2 != 0 && dl1 < 20)
            {
                textBox1.Text = back.remove(textBox1.Text);
                textBox1.Text += "3";
                m--;
            }
            else if (dl1 < 20 && sign != "=")
            {
                textBox1.Text += "3";
            }

            if (sign != "" && pressAction == 1 && point == 1 && dl1 < 20)
            {
                label1.Text = "3";
            }

            int dl3 = label1.Text.Length;

            pressAction = 0;

            label1.Font = resize.resizeFont(dl3);
        }

        private void four_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (label1.Text == "0" && textBox1.Text != "")
            {
                textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                textBox1.Text += "4";
                label1.Text = "4";
            }
            else if (label1.Text == "0" && dl2 == 0 && textBox1.Text != "" || label1.Text == "Nieprawidłowe dane wejścia" || textBox1.Text == "0")
            {
                label1.Text = "4";
                textBox1.Text = "4";
            }
            else if (dl1 < 27 && sign != "=")
            {
                label1.Text += "4";
            }

            if (label1.Text == "Nieprawidłowe dane wejścia" || sign == "=")
            {
                clear_Click(sender, e);
                label1.Text = "4";
            }

            if (point != 0 && label1.Text == "4" && m > 0 && dl2 != 0 && dl1 < 20)
            {
                textBox1.Text = back.remove(textBox1.Text);
                textBox1.Text += "4";
                m--;
            }
            else if (dl1 < 20 && sign != "=")
            {
                textBox1.Text += "4";
            }

            if (sign != "" && pressAction == 1 && point == 1 && dl1 < 20)
            {
                label1.Text = "4";
            }

            int dl3 = label1.Text.Length;

            pressAction = 0;

            label1.Font = resize.resizeFont(dl3);
        }

        private void five_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (label1.Text == "0" && textBox1.Text != "")
            {
                textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                textBox1.Text += "5";
                label1.Text = "5";
            }
            else if (label1.Text == "0" && dl2 == 0 && textBox1.Text != "" || label1.Text == "Nieprawidłowe dane wejścia" || textBox1.Text == "0")
            {
                label1.Text = "5";
                textBox1.Text = "5";
            }
            else if (dl1 < 27 && sign != "=")
            {
                label1.Text += "5";
            }

            if (label1.Text == "Nieprawidłowe dane wejścia" || sign == "=")
            {
                clear_Click(sender, e);
                label1.Text = "5";
            }

            if (point != 0 && label1.Text == "5" && m > 0 && dl2 != 0 && dl1 < 20)
            {
                textBox1.Text = back.remove(textBox1.Text);
                textBox1.Text += "5";
                m--;
            }
            else if (dl1 < 20 && sign != "=")
            {
                textBox1.Text += "5";
            }

            if (sign != "" && pressAction == 1 && point == 1 && dl1 < 20)
            {
                label1.Text = "5";
            }

            int dl3 = label1.Text.Length;

            pressAction = 0;

            label1.Font = resize.resizeFont(dl3);
        }

        private void six_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (label1.Text == "0" && textBox1.Text != "")
            {
                textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                textBox1.Text += "6";
                label1.Text = "6";
            }
            else if (label1.Text == "0" && dl2 == 0 && textBox1.Text != "" || label1.Text == "Nieprawidłowe dane wejścia" || textBox1.Text == "0")
            {
                label1.Text = "6";
                textBox1.Text = "6";
            }
            else if (dl1 < 27 && sign != "=")
            {
                label1.Text += "6";
            }

            if (label1.Text == "Nieprawidłowe dane wejścia" || sign == "=")
            {
                clear_Click(sender, e);
                label1.Text = "6";
            }

            if (point != 0 && label1.Text == "6" && m > 0 && dl2 != 0 && dl1 < 20)
            {
                textBox1.Text = back.remove(textBox1.Text);
                textBox1.Text += "6";
                m--;
            }
            else if (dl1 < 20 && sign != "=")
            {
                textBox1.Text += "6";
            }

            if (sign != "" && pressAction == 1 && point == 1 && dl1 < 20)
            {
                label1.Text = "6";
            }

            int dl3 = label1.Text.Length;

            pressAction = 0;

            label1.Font = resize.resizeFont(dl3);
        }

        private void seven_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (label1.Text == "0" && textBox1.Text != "")
            {
                textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                textBox1.Text += "7";
                label1.Text = "7";
            }
            else if (label1.Text == "0" && dl2 == 0 && textBox1.Text != "" || label1.Text == "Nieprawidłowe dane wejścia" || textBox1.Text == "0")
            {
                label1.Text = "7";
                textBox1.Text = "7";
            }
            else if (dl1 < 27 && sign != "=")
            {
                label1.Text += "7";
            }

            if (label1.Text == "Nieprawidłowe dane wejścia" || sign == "=")
            {
                clear_Click(sender, e);
                label1.Text = "7";
            }

            if (point != 0 && label1.Text == "7" && m > 0 && dl2 != 0 && dl1 < 20)
            {
                textBox1.Text = back.remove(textBox1.Text);
                textBox1.Text += "7";
                m--;
            }
            else if (dl1 < 20 && sign != "=")
            {
                textBox1.Text += "7";
            }

            if (sign != "" && pressAction == 1 && point == 1 && dl1 < 20)
            {
                label1.Text = "7";
            }

            int dl3 = label1.Text.Length;

            pressAction = 0;

            label1.Font = resize.resizeFont(dl3);
        }

        private void eight_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (label1.Text == "0" && textBox1.Text != "")
            {
                textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                textBox1.Text += "8";
                label1.Text = "8";
            }
            else if (label1.Text == "0" && dl2 == 0 && textBox1.Text != "" || label1.Text == "Nieprawidłowe dane wejścia" || textBox1.Text == "0")
            {
                label1.Text = "8";
                textBox1.Text = "8";
            }
            else if (dl1 < 27 && sign != "=")
            {
                label1.Text += "8";
            }

            if (label1.Text == "Nieprawidłowe dane wejścia" || sign == "=")
            {
                clear_Click(sender, e);
                label1.Text = "8";
            }

            if (point != 0 && label1.Text == "8" && m > 0 && dl2 != 0 && dl1 < 20)
            {
                textBox1.Text = back.remove(textBox1.Text);
                textBox1.Text += "8";
                m--;
            }
            else if (dl1 < 20 && sign != "=")
            {
                textBox1.Text += "8";
            }

            if (sign != "" && pressAction == 1 && point == 1 && dl1 < 20)
            {
                label1.Text = "8";
            }

            int dl3 = label1.Text.Length;

            pressAction = 0;

            label1.Font = resize.resizeFont(dl3);
        }

        private void nine_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;

            if (label1.Text == "0" && textBox1.Text != "")
            {
                textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                textBox1.Text += "9";
                label1.Text = "9";
            }
            else if (label1.Text == "0" && dl2 == 0 && textBox1.Text != "" || label1.Text == "Nieprawidłowe dane wejścia" || textBox1.Text == "0")
            {
                label1.Text = "9";
                textBox1.Text = "9";
            }
            else if (dl1 < 27 && sign != "=")
            {
                label1.Text += "9";
            }

            if (label1.Text == "Nieprawidłowe dane wejścia" || sign == "=")
            {
                clear_Click(sender, e);
                label1.Text = "9";
            }

            if (point != 0 && label1.Text == "9" && m > 0 && dl2 != 0 && dl1 < 20)
            {
                textBox1.Text = back.remove(textBox1.Text);
                textBox1.Text += "9";
                m--;
            }
            else if (dl1 < 20 && sign != "=")
            {
                textBox1.Text += "9";
            }

            if (sign != "" && pressAction == 1 && point == 1 && dl1 < 20)
            {
                label1.Text = "9";
            }

            int dl3 = label1.Text.Length;

            pressAction = 0;

            label1.Font = resize.resizeFont(dl3);
        }      

        private void zero_Click(object sender, EventArgs e)
        {
            int dl1 = label1.Text.Length;
            int dl2 = textBox1.Text.Length;    

            if (pressAction == 0 && point == 0)  
            {
                label1.Text += "0";
                textBox1.Text += "0";              
            }
            else if (textBox1.Text == "" && zeroOne > 0) 
            {
                textBox1.Text += "0";
                zeroOne--;            
            }
            else if (pressAction == 1 && point == 0 && dl1 < 20 && sign != "=") 
            {
                label1.Text += "0";
                textBox1.Text += "0";
            }
            else if (textBox1.Text != "" && pressAction == 1 && dl1 < 20 && sign != "=")   
            {
                label1.Text = "0";
                textBox1.Text += "0";              
            }
            else if (pressAction == 0 && point == 1 && label1.Text != "0")  
            {
                label1.Text += "0";
                textBox1.Text += "0";
            }

            if (label1.Text == "Nieprawidłowe dane wejścia" || sign == "=")
            {
                clear_Click(sender, e);
                textBox1.Text = "0";
            }

            pressAction = 0;
         
        }

        private void comma_Click(object sender, EventArgs e)
        {       
            if (label1.Text == "0" && textBox1.Text == "" && point > 0)
            {
                label1.Text += ",";
                textBox1.Text += "0,";
                point--;
            } 
            else if (pressAction == 1 && point > 0 && textBox1.Text == "0")
            {
                label1.Text = "0,";
                textBox1.Text += ",";
                point--;
            } 
            else if (pressAction == 0 && point > 0 && sign != "√" && sign != "sqr" && sign != "!" && sign != "%" && sign != "log" && sign != "pi")
            {
                label1.Text += ",";
                textBox1.Text += ",";
                point--;
            }
            else if (point > 0 && sign != "√" && sign != "sqr" && sign != "!" && sign != "%" && sign != "log" && sign != "pi")
            {
                label1.Text = "0,";
                textBox1.Text += "0,";
                point--;
            }
        }

        private void clear_Click(object sender, EventArgs e)
        {
            zeroOne = 1;
            point = 1;
            m = 1;
            pressAction = 1;
            firstTime = 1;
            score = 0;
            temporary = 1;
            number1 = 0;

            multiplication.Enabled = divide.Enabled = plus.Enabled = minus.Enabled =
            root.Enabled = factorial.Enabled = percent.Enabled = result.Enabled = logarithm.Enabled =
            CE.Enabled = pi.Enabled = comma.Enabled = one.Enabled = square.Enabled = backspace.Enabled =
            two.Enabled = three.Enabled = four.Enabled = five.Enabled =
            six.Enabled = seven.Enabled = eight.Enabled = nine.Enabled = zero.Enabled = true;

            clear.BackColor = Color.FromArgb(226,226,226);

            font = label1.Font.Size;
            font = 30;
            label1.Font = new Font(label1.Font.Name, font,
            label1.Font.Style, label1.Font.Unit);

            label1.Text = "0";
            textBox1.Text = "";
            sign = " ";
        }

        private void backspace_Click(object sender, EventArgs e)  
        {
            int dl1 = label1.Text.Length ;
            int dl2 = textBox1.Text.Length ;    

            if (pressAction == 0 && dl1 != 0 && dl2 != 0 && sign != "sqr" && sign != "√" && sign != "!" && sign != "%" && sign != "log")
            {                            
                if (label1.Text == "0")
                {
                    textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);                                  
                    point = 1;                   
                }
                else
                {
                    label1.Text = label1.Text.Remove(dl1 - 1, 1);
                    textBox1.Text = textBox1.Text.Remove(dl2 - 1, 1);
                }
             
            }

            if (dl1 == 1) 
            {
                label1.Text = "0";
                pressAction = 1;        
            }

            int dl3 = label1.Text.Length;

            label1.Font = resize.resizeFont(dl3);

        } 

        private void result_Click(object sender, EventArgs e) 
        {
            numberTxt = label1.Text;
            number1 = Convert.ToDouble(numberTxt);

            if (sign != "" && pressAction == 1)
            {
                numberTxt = Convert.ToString(score);
                label1.Text = numberTxt;
            }
            else if (sign != "" && pressAction == 0)
            {
                score = calculations.calculating(sign, score, number1);
                numberTxt = Convert.ToString(score);
                label1.Text = numberTxt;
            }

            numberTxt = label1.Text;
            number1 = Convert.ToDouble(numberTxt);

            if (sign == "÷")
            {
                if (number1 == 0)
                {
                    font = label1.Font.Size;
                    font -= 18;
                    label1.Font = new Font(label1.Font.Name, font,
                    label1.Font.Style, label1.Font.Unit);
                    label1.Text = "Nieprawidłowe dane wejścia";

                    warning(sender, e);
                }
                else
                {                   
                    numberTxt = Convert.ToString(score);
                    label1.Text = numberTxt; 
                }
            }

            textBox1.Text = "";
            zeroOne = 1;
            point = 1;
            m = 1;
            pressAction = 1;
            temporary = 1;
            number1 = 0;
            sign = "=";
        }  

    }
}